// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserStore on _UserStore, Store {
  final _$curantUserAtom = Atom(name: '_UserStore.curantUser');

  @override
  ObservableFuture<User> get curantUser {
    _$curantUserAtom.reportRead();
    return super.curantUser;
  }

  @override
  set curantUser(ObservableFuture<User> value) {
    _$curantUserAtom.reportWrite(value, super.curantUser, () {
      super.curantUser = value;
    });
  }

  final _$updateUserAtom = Atom(name: '_UserStore.updateUser');

  @override
  Observable<bool> get updateUser {
    _$updateUserAtom.reportRead();
    return super.updateUser;
  }

  @override
  set updateUser(Observable<bool> value) {
    _$updateUserAtom.reportWrite(value, super.updateUser, () {
      super.updateUser = value;
    });
  }

  final _$dataUserActivateAtom = Atom(name: '_UserStore.dataUserActivate');

  @override
  Observable<SocketResponse> get dataUserActivate {
    _$dataUserActivateAtom.reportRead();
    return super.dataUserActivate;
  }

  @override
  set dataUserActivate(Observable<SocketResponse> value) {
    _$dataUserActivateAtom.reportWrite(value, super.dataUserActivate, () {
      super.dataUserActivate = value;
    });
  }

  final _$_UserStoreActionController = ActionController(name: '_UserStore');

  @override
  void updateData(SocketResponse mapData) {
    final _$actionInfo =
        _$_UserStoreActionController.startAction(name: '_UserStore.updateData');
    try {
      return super.updateData(mapData);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void switchUpdateUser() {
    final _$actionInfo = _$_UserStoreActionController.startAction(
        name: '_UserStore.switchUpdateUser');
    try {
      return super.switchUpdateUser();
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void getCurantUser() {
    final _$actionInfo = _$_UserStoreActionController.startAction(
        name: '_UserStore.getCurantUser');
    try {
      return super.getCurantUser();
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
curantUser: ${curantUser},
updateUser: ${updateUser},
dataUserActivate: ${dataUserActivate}
    ''';
  }
}
