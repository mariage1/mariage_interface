import 'package:mariage_ui/data/model_users/user.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/data/response/socket_response.dart';
import 'package:mariage_ui/service/graphql/query/graphql_profil_query.dart';
import 'package:mobx/mobx.dart';

// flutter packages pub run build_runner build --delete-conflicting-outputs
part 'user_store.g.dart';

class UserStore = _UserStore with _$UserStore;

abstract class _UserStore with Store {
  final GraphqlProfilQuery _graphqlProfilQuery =  GraphqlProfilQuery.instance;

  @observable
  ObservableFuture<User> curantUser =
      ObservableFuture<User>.value(null);

  @observable
  Observable<bool> updateUser = Observable(false);

  @observable
  Observable<SocketResponse> dataUserActivate = Observable(SocketResponse(stat: 'init'));

  @action
  void updateData(SocketResponse mapData){
    dataUserActivate = Observable(mapData);
  }

  @action
  void switchUpdateUser() {
    updateUser = Observable(!updateUser.value);
  }

  @action
  void getCurantUser() {
    curantUser = ObservableFuture<User>(_graphqlProfilQuery.me());
  }
}
