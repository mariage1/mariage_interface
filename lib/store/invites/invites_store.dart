import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/data/model_users/list_invites.dart';
import 'package:mariage_ui/service/graphql/query/graphql_invite_query.dart';
import 'package:mobx/mobx.dart';

part 'invites_store.g.dart';

class InvitesStore = _InvitesStore with _$InvitesStore;

abstract class _InvitesStore with Store {
  final GraphqlInviteQuery _graphqlInviteQuery = GraphqlInviteQuery.instance;

  @observable
  ObservableFuture<ListInvites> listInvites = ObservableFuture.value(null);

  @observable
  ObservableFuture<Invite> invite = ObservableFuture<Invite>.value(null);

  @action
  Future<void> loadingInvites() => listInvites = ObservableFuture(_graphqlInviteQuery.queryInvitesInfo().then((value) => value));

  @action
  Future<void> getInvite(String uuid) => invite = ObservableFuture(_graphqlInviteQuery.queryInvite(uuid).then((value) => value));
}
