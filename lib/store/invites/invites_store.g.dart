// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invites_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$InvitesStore on _InvitesStore, Store {
  final _$listInvitesAtom = Atom(name: '_InvitesStore.listInvites');

  @override
  ObservableFuture<ListInvites> get listInvites {
    _$listInvitesAtom.reportRead();
    return super.listInvites;
  }

  @override
  set listInvites(ObservableFuture<ListInvites> value) {
    _$listInvitesAtom.reportWrite(value, super.listInvites, () {
      super.listInvites = value;
    });
  }

  final _$inviteAtom = Atom(name: '_InvitesStore.invite');

  @override
  ObservableFuture<Invite> get invite {
    _$inviteAtom.reportRead();
    return super.invite;
  }

  @override
  set invite(ObservableFuture<Invite> value) {
    _$inviteAtom.reportWrite(value, super.invite, () {
      super.invite = value;
    });
  }

  final _$_InvitesStoreActionController =
      ActionController(name: '_InvitesStore');

  @override
  Future<void> loadingInvites() {
    final _$actionInfo = _$_InvitesStoreActionController.startAction(
        name: '_InvitesStore.loadingInvites');
    try {
      return super.loadingInvites();
    } finally {
      _$_InvitesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<void> getInvite(String uuid) {
    final _$actionInfo = _$_InvitesStoreActionController.startAction(
        name: '_InvitesStore.getInvite');
    try {
      return super.getInvite(uuid);
    } finally {
      _$_InvitesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listInvites: ${listInvites},
invite: ${invite}
    ''';
  }
}
