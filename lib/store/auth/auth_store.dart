import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mobx/mobx.dart';

// flutter packages pub run build_runner build --delete-conflicting-outputs
part 'auth_store.g.dart';

class AuthStore = _AuthStore with _$AuthStore;

abstract class _AuthStore with Store {

  final AuthService _authService = AuthService.instance;

  @observable
  ObservableFuture<bool> isAuth = ObservableFuture<bool>(AuthService.instance.isAuth());


  @action
  Future<ApiResponse> login(String email, String password) async {
    final apiResponse = await _authService.loginService(email, password);
    isAuth = ObservableFuture<bool>.value(apiResponse.stat);
    return apiResponse;
  }

  @action
  Future<void> logout() async {
    await _authService.logout();
    isAuth = ObservableFuture<bool>.value(false);
  }

}