import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';

class LoadingGifAnimation extends StatefulWidget {
  const LoadingGifAnimation({Key key}) : super(key: key);

  @override
  _LoadingGifAnimationState createState() => _LoadingGifAnimationState();
}

class _LoadingGifAnimationState extends State<LoadingGifAnimation>
    with TickerProviderStateMixin {
  GifController controller;

  @override
  void initState() {
    // TODO: implement initState
    controller = GifController(vsync: this);
    controller.repeat(
        min: 0, max: 120, period: const Duration(milliseconds: 900));
    super.initState();
  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    controller.stop();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return GifImage(
      controller: controller,
      image: const AssetImage('images/marriage-couple.gif'),
    );
  }
}
