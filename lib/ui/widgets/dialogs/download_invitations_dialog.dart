import 'package:flutter/material.dart';
import 'package:mariage_ui/service/logger_service.dart';

class DownloadInvitationsDialog extends StatefulWidget {
  const DownloadInvitationsDialog({Key key, @required this.downloadFunction}) : super(key: key);

  final Function downloadFunction;

  @override
  _DownloadInvitationsDialogState createState() => _DownloadInvitationsDialogState();
}

class _DownloadInvitationsDialogState extends State<DownloadInvitationsDialog> {

  bool _generat;
  LoggerService _loggerService;

  @override
  void initState() {
    // TODO: implement initState
    _generat = false;
    _loggerService = LoggerService.instance;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Téléchargement des QR code d'Invitations"),
      content: Row(children: [
        const Text('Voulais vous generé les QR code ?'),
        Switch(
          value: _generat,
          onChanged: (bool value) {
            setState(() {
              _generat = value;
            });
            _loggerService.debug('$_generat');
          },
        )
      ]),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('CANCEL'),
        ),
        TextButton(
          onPressed: () {
            widget.downloadFunction(_generat);
            Navigator.pop(context);
          },
          child: const Text('ACCEPT'),
        ),
      ],
    );
  }
}
