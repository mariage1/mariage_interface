import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/store/invites/invites_store.dart';
import 'package:mariage_ui/ui/widgets/forms/form_invite.dart';

import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

class InviteFormDialogue extends StatefulWidget {
  const InviteFormDialogue({Key key, this.uuid, @required this.newInvite})
      : super(key: key);
  final String uuid;
  final bool newInvite;

  @override
  _InviteFormDialogue createState() => _InviteFormDialogue();
}

class _InviteFormDialogue extends State<InviteFormDialogue>  {
  InvitesStore _invitesStore;
  Invite _invite;

  bool _newInvite;

  @override
  void initState() {
    _invitesStore = Provider.of<InvitesStore>(context, listen: false);
    _newInvite = widget.newInvite;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!_newInvite) {
      _invitesStore.getInvite(widget.uuid);
    }
    Widget _widget;
    return AlertDialog(
        title: const Text('Update Invites'),
        content: Observer(builder: (_) {
          switch (_invitesStore.invite.status) {
            case FutureStatus.pending:
              _widget = Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const [LoadingGifAnimation()]);
              break;
            case FutureStatus.rejected:
              _widget = const Text('Error');
              break;
            case FutureStatus.fulfilled:
              if (!_newInvite) {
                _invite = _invitesStore.invite.value;
              } else {
                _invite = Invite();
              }
              _widget = FormInvite(invite: _invite, newInvite: widget.newInvite);
              break;
          }
          return _widget;
        }));
  }
}
