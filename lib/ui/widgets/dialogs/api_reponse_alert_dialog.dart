import 'package:flutter/material.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';

class ApiResponseAlertDialog extends StatefulWidget {
  const ApiResponseAlertDialog({Key key, this.response, this.titre})
      : super(key: key);
  final Future<ApiResponse> response;
  final String titre;
  @override
  _ApiResponseAlertDialogState createState() => _ApiResponseAlertDialogState();
}

class _ApiResponseAlertDialogState extends State<ApiResponseAlertDialog> {

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.titre),
      content: SingleChildScrollView(
          child: FutureBuilder(
        future: widget.response,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final res = snapshot.data;
            return ListBody(
              children: <Widget>[
                Text('Stat : ${res._stat}'),
                Text('Message :  ${res.message}'),
              ],
            );
          } else {
            return Column(
                mainAxisSize: MainAxisSize.min,
                children: const [LoadingGifAnimation()]);
          }
        },
      )),
      actions: <Widget>[
        FutureBuilder(
          future: widget.response,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Approve'),
              );
            } else {
              return const TextButton(
                onPressed: null,
                child: Text('Approve'),
              );
            }
          },
        )
      ],
    );
  }
}
