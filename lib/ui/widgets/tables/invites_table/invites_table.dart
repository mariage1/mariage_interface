import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_invite_mutation.dart';
import 'package:mariage_ui/store/invites/invites_store.dart';
import 'package:mariage_ui/ui/widgets/dialogs/invite_update_dialoge.dart';
import 'package:provider/provider.dart';

import 'invites_data_source.dart';

class InvitesTable extends StatefulWidget {
  const InvitesTable({Key key, @required this.invites}) : super(key: key);

  final List<Invite> invites;

  @override
  InvitesTableState createState() => InvitesTableState();
}

class InvitesTableState extends State<InvitesTable> {
  int selectedIndex = -1;
  bool sort;
  InviteTableDataSource _dataSource;
  final List<Widget> _actions = [];

  List<Invite> _currentInvites;

  bool _sortByFamille;
  bool _sortByFirstName;
  bool _sortByValidated;
  bool _sortByLastName;
  bool _sortByEmail;

  IconButton _singelUpdate;
  IconButton _groupUpdate;
  IconButton _delete;

  GraphqlInviteMutation _graphqlMutationInvite;
  InvitesStore _invitesStore;

  int _indexSort = 0;

  @override
  void initState() {
    _graphqlMutationInvite = GraphqlInviteMutation.instance;
    _invitesStore = Provider.of<InvitesStore>(context, listen: false);

    sort = false;
    _sortByFirstName = false;
    _sortByFamille = false;
    _sortByValidated = false;
    _sortByLastName = false;
    _sortByEmail = false;

    _indexSort = null;

    _currentInvites = widget.invites;

    _singelUpdate = IconButton(
      tooltip: "Modifier l'invite selectioner",
      icon: const Icon(Icons.person),
      onPressed: _showUpdateInviteDialog,
    );

    _groupUpdate = IconButton(
      icon: const Icon(Icons.group),
      onPressed: () => {},
    );

    _delete = IconButton(
      icon: const Icon(Icons.delete),
      onPressed: _deleteSelectedInvite,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _dataSource = InviteTableDataSource(context, _currentInvites, _actions);
    _dataSource.addListener(_iconActionController);

    return PaginatedDataTable(
        actions: _actions,
        sortAscending: sort,
        sortColumnIndex: _indexSort,
        header: const Text('Liste des invites'),
        columns: [
          DataColumn(
              onSort: (columnIndex, ascending) {
                setState(() {
                  sort = !_sortByValidated;
                  _indexSort = columnIndex;
                });
                _sortByValidated = !_sortByValidated;
                _onSortColum(columnIndex, _sortByValidated);
              },
              label: const Text('Infos')),
          DataColumn(
            onSort: (columnIndex, ascending) {
              setState(() {
                sort = !_sortByFirstName;
                _indexSort = columnIndex;
              });
              _sortByFirstName = !_sortByFirstName;
              _onSortColum(columnIndex, _sortByFirstName);
            },
            label: const Text(
              'FirstName',
            ),
          ),
          DataColumn(
              label: const Text(
                'LastName',
              ),
              onSort: (columnIndex, ascending) {
                setState(() {
                  sort = !_sortByLastName;
                  _indexSort = columnIndex;
                });
                _sortByLastName = !_sortByLastName;
                _onSortColum(columnIndex, _sortByLastName);
              }),
          DataColumn(
              label: const Text('email'),
              onSort: (columnIndex, ascending) {
                setState(() {
                  sort = !_sortByEmail;
                  _indexSort = columnIndex;
                });
                _sortByEmail = !_sortByEmail;
                _onSortColum(columnIndex, _sortByEmail);
              }),
          DataColumn(
            label: const Text('Famille'),
            onSort: (columnIndex, ascending) {
              setState(() {
                sort = !_sortByFamille;
                _indexSort = columnIndex;
              });
              _sortByFamille = !_sortByFamille;
              _onSortColum(columnIndex, _sortByFamille);
            },
          ),
        ],
        showCheckboxColumn: false,
        source: _dataSource);
  }

  Future<void> _deleteSelectedInvite() async {
    final uuids = _dataSource.uuidSelectedRow;
    await _graphqlMutationInvite.mutationDeleteInvite(uuids).then((value) =>
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(seconds: 2),
            content: Text(value.message))));
    await _invitesStore.loadingInvites();
  }

  void _onSortColum(int columnIndex, bool ascending) {
    switch (columnIndex) {
      case 0:
        ascending
            ? _currentInvites.sort((a, b) {
                if (a.hasValidate == b.hasValidate) {
                  return 0;
                }
                return a.hasValidate ? -1 : 1;
              })
            : _currentInvites.sort((a, b) {
                if (a.hasValidate == b.hasValidate) {
                  return 0;
                }
                return a.hasValidate ? 1 : -1;
              });
        break;
      case 1:
        ascending
            ? _currentInvites.sort((a, b) {
                return a.firstName.compareTo(b.firstName);
              })
            : _currentInvites.sort((a, b) {
                return b.firstName.compareTo(a.firstName);
              });
        break;
      case 2:
        ascending
            ? _currentInvites.sort((a, b) {
                return a.lastName.compareTo(b.lastName);
              })
            : _currentInvites.sort((a, b) {
                return b.lastName.compareTo(a.lastName);
              });
        break;
      case 3:
        ascending
            ? _currentInvites.sort((a, b) {
                return a.email.compareTo(b.email);
              })
            : _currentInvites.sort((a, b) {
                return b.email.compareTo(a.email);
              });
        break;
      case 4:
        ascending
            ? _currentInvites.sort((a, b) {
                return a.famille.name.compareTo(b.famille.name);
              })
            : _currentInvites.sort((a, b) {
                return b.famille.name.compareTo(a.famille.name);
              });
        break;
    }
  }

  Future<void> _showUpdateInviteDialog() async {
    final uuid = _dataSource.uuidSelectedRow;
    if (_dataSource.selectedRowCount == 1) {
      return showDialog<void>(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context) {
            return InviteFormDialogue(
              uuid: uuid.first,
              newInvite: false,
            );
          });
    }
  }

  void _iconActionController() {
    if (_dataSource.selectedRowCount > 0) {
      if (_dataSource.iconeAction.isEmpty) {
        _dataSource.iconeAction.addAll([_singelUpdate, _delete]);
      } else {
        if (_dataSource.selectedRowCount == 1) {
          _dataSource.iconeAction.replaceRange(0, 1, [_singelUpdate]);
        } else {
          _dataSource.iconeAction.replaceRange(0, 1, [_groupUpdate]);
        }
      }
    } else if (_dataSource.selectedRowCount == 0) {
      _dataSource.iconeAction.clear();
    }
  }
}
