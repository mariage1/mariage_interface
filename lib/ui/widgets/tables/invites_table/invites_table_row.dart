import 'package:flutter/material.dart';

class InviteTableRow {
  InviteTableRow(this.infos, this.firstName, this.lastName, this.email, this.familleName,
      this.uuid);

  final List<Icon> infos;
  final String firstName;
  final String lastName;
  final String email;
  final String familleName;
  final String uuid;

  bool selected = false;
}