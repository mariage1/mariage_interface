import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'invites_table_row.dart';


class InviteTableDataSource extends DataTableSource {
  InviteTableDataSource(
      this.context, List<Invite> listInvites, this.iconeAction) {
    _rows = listInvites
        .map((e) => InviteTableRow(_buildInfo(e.hasValidate, e.categorie, e.importance), e.firstName,
            e.lastName, e.email, e.famille.name, e.uuid))
        .toList();
  }

  List<Icon> _buildInfo(bool hasValidate, String categorie, String importance) {
    final _infoIcons = <Icon>[];

    hasValidate
        ? _infoIcons.add(Icon(
            Icons.done,
            color: Theme.of(context).primaryColorLight,
          ))
        : _infoIcons
            .add(Icon(Icons.close, color: Theme.of(context).primaryColorDark));

    switch(categorie){
      case 'Famille':
        _infoIcons.add(Icon(
          Icons.family_restroom,
          color: Theme.of(context).primaryColorLight,
        ));
        break;
      case 'Amis':
        _infoIcons.add(Icon(
          Icons.accessibility_new_outlined,
          color: Theme.of(context).primaryColorLight,
        ));
        break;
      case 'Autre':
        _infoIcons.add(Icon(
          Icons.perm_identity,
          color: Theme.of(context).primaryColorLight,
        ));
        break;
    }

    switch(importance){
      case 'TI':
        _infoIcons.add(Icon(
          Icons.local_fire_department,
          color: Theme.of(context).primaryColorDark,
        ));
        break;
      case 'I':
        _infoIcons.add(Icon(
          Icons.warning,
          color: Theme.of(context).primaryColor,
        ));
        break;
      case 'F':
        _infoIcons.add(Icon(
          Icons.lightbulb,
          color: Theme.of(context).primaryColorLight,
        ));
        break;
    }


    return _infoIcons;
  }

  final BuildContext context;
  List<InviteTableRow> _rows;
  List<Widget> iconeAction;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) {
      return null;
    }
    final row = _rows[index];

    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(Row(children:  row.infos)),
        DataCell(Text(row.firstName)),
        DataCell(Text(row.lastName)),
        DataCell(Text(row.email)),
        DataCell(Text(row.familleName))
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  List<String> get uuidSelectedRow =>
      _rows.where((element) => element.selected).map((e) => e.uuid).toList();
}
