import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/invites.dart';

import 'famille_table_row.dart';


class FamilleTableDataSource extends DataTableSource {

  FamilleTableDataSource(
      this.context,
      List<Invite> listeMembre,
      this._action,
      this._updateFonction
      ){
      _rows = listeMembre.map((e) => FamilleTableRow(e.firstName, e.lastName, e.hasValidate, e.uuid)).toList();
  }

  final BuildContext context;
  List<FamilleTableRow> _rows;
  final bool _action;
  final Function _updateFonction;
  final int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if(index >= _rows.length){
      return null;
    }
    final row = _rows[index];

    return DataRow.byIndex(
        index: index,
        cells: [
          DataCell(Text('${row.firstName} ${row.lastName}')),
          DataCell(DropdownButton(
            value: row.validate,
            items: const [
              DropdownMenuItem(
                value: true,
                child: Text('viens'),
              ),
              DropdownMenuItem(
                value: false,
                child: Text('ne viens pas'),
              ),
            ],
            onChanged: _action ? (value) {
              row.updateValidate();
              _updateFonction(index);
              notifyListeners();
            } : null,
          ))
        ]);


  }

  @override
  // TODO: implement isRowCountApproximate
  bool get isRowCountApproximate => false;

  @override
  // TODO: implement rowCount
  int get rowCount => _rows.length;

  @override
  // TODO: implement selectedRowCount
  int get selectedRowCount => _selectedCount;

  List<FamilleTableRow> get rows => _rows;

}