class FamilleTableRow {
  FamilleTableRow( this.firstName, this.lastName, this.validate, this.uuid);

  final String firstName;
  final String lastName;
  bool validate;
  final String uuid;

  void updateValidate(){
    validate = !validate;
  }

}