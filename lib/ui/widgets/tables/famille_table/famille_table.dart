import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/famille.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_famille_mutation.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_invite_mutation.dart';
import 'package:mariage_ui/service/logger_service.dart';

import 'famille_data_source.dart';

class FamilleTable extends StatefulWidget {
  const FamilleTable({Key key, @required this.famille}) : super(key: key);

  final Famille famille;

  @override
  _FamilleTableState createState() => _FamilleTableState();
}

class _FamilleTableState extends State<FamilleTable> {
  @override
  void initState() {
    _famille = widget.famille;

    _editIconButton = IconButton(
      icon: const Icon(Icons.edit_outlined),
      onPressed: _editAction,
    );

    _annulIconButton = IconButton(
      icon: const Icon(Icons.close),
      onPressed: _closeAction,
    );

    _validIconButton = IconButton(
      icon: const Icon(Icons.done),
      onPressed: _doneAction,
    );

    _active = false;
    _actions = [_editIconButton];

    _membres = _famille.membres;
    initNameFamily = _famille.name;

    _graphqlFamilleMutation = GraphqlFamilleMutation.instance;

    super.initState();
  }

  GraphqlFamilleMutation _graphqlFamilleMutation;

  Famille _famille;
  IconButton _editIconButton;
  IconButton _annulIconButton;
  IconButton _validIconButton;

  List<Widget> _actions;

  bool _active;

  FamilleTableDataSource _dataSource;
  String initNameFamily;

  List<Invite> _membres;
  final List<int> _updatIndex = [];
  final _formKey = GlobalKey<FormState>();

  GraphqlInviteMutation _graphqlInviteMutation;

  @override
  Widget build(BuildContext context) {
    _graphqlInviteMutation = GraphqlInviteMutation.instance;
    _dataSource = FamilleTableDataSource(
        context, _membres, _active, updateMembrePresance);

    return PaginatedDataTable(
      showCheckboxColumn: false,
      actions: _actions,
      header: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Container(),
          ),
          Expanded(
              flex: 6,
              child: Form(
                  key: _formKey,
                  child: TextFormField(
                      initialValue: _famille.name,
                      onChanged: (val) => _famille.name = val,
                      enabled: _active,
                      maxLength: 30,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.family_restroom_outlined),
                        labelText: 'Famille Name',
                        helperText: 'Famille Name',
                      ),
                      validator: _textFieldValidator))),
          Expanded(child: Container()),
          Expanded(
              child: TextFormField(
            initialValue: _famille.membres.length.toString(),
            enabled: false,
            decoration: const InputDecoration(
              labelText: 'membre',
              helperText: 'membre',
            ),
          )),
          Expanded(
            child: Container(),
          ),
        ],
      ),
      rowsPerPage: 5,
      columns: const [
        DataColumn(label: Text('Membre')),
        DataColumn(label: Text('Présence'))
      ],
      source: _dataSource,
    );
  }

  void updateMembrePresance(int index) {
    _membres[index].hasValidate = !_membres[index].hasValidate;
    _updatIndex.add(index);
  }

  void _editAction() {
    setState(() {
      _active = true;
      _actions = [_annulIconButton, _validIconButton];
    });
  }

  void _closeAction() {
    _formKey.currentState.reset();

    setState(() {
      for(final index in _updatIndex){
        _membres[index].hasValidate = !_membres[index].hasValidate;
      }
      _dataSource.notifyListeners();
      _active = false;
      _actions = [_editIconButton];
    });
  }

  void _doneAction() {
    if (_formKey.currentState.validate()) {
      if(_famille.name != initNameFamily){
        LoggerService.instance.debug('TODO: update famille name ${_famille.name}');
        _graphqlFamilleMutation.mutationFamille(_famille);
        initNameFamily = _famille.name;
      }

    } else {
      return;
    }

    for (final index in _updatIndex) {
      final invite = Invite(
          uuid: _membres[index].uuid, hasValidate: _membres[index].hasValidate);
      _graphqlInviteMutation.mutationInvite(invite, null);
    }

    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        duration: Duration(seconds: 3), content: Text('Update')));

    setState(() {
      _active = false;
      _actions = [_editIconButton];
    });
  }

  String _textFieldValidator(dynamic value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}
