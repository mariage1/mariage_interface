import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/famille.dart';
import 'package:mariage_ui/ui/widgets/tables/famille_table/famille_table.dart';


class FamilyCard extends StatefulWidget {
  const FamilyCard({Key key, @required this.famille}) : super(key: key);

  final Famille famille;

  @override
  State<StatefulWidget> createState() => _FamilyCardState();
}

class _FamilyCardState extends State<FamilyCard> {
  Famille _famille;

  @override
  void initState() {
    _famille = widget.famille;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  Card(
            margin: const EdgeInsets.all(25),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              ListTile(
                title: const Text('Famille'),
                tileColor: Theme.of(context).primaryColorLight,
                subtitle: const Text('Information de votre famille'),
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  Expanded(
                    flex: 8,
                    child: FamilleTable(
                      famille: _famille,
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                ],
              ),
            ]));
  }
}
