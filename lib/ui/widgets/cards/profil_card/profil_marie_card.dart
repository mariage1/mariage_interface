import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/marie.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_marie_mutation.dart';
import 'package:mariage_ui/ui/widgets/forms/update_action_bar.dart';

class ProfilMarieCard extends StatefulWidget {
  const ProfilMarieCard({Key key, @required this.marie}) : super(key: key);

  final Marie marie;

  @override
  State<StatefulWidget> createState() => _ProfilMarieCardState();
}

class _ProfilMarieCardState extends State<ProfilMarieCard> {
  final _formKey = GlobalKey<FormState>();

  GraphqlMarieMutation _graphqlMarieMutation;

  Marie _marie;

  bool _updateMode;

  String _firsName, _lastName, _email;

  @override
  void initState() {
    _graphqlMarieMutation = GraphqlMarieMutation.instance;

    _updateMode = false;

    _marie = widget.marie;
    _firsName = _marie.firstName;
    _lastName = _marie.lastName;
    _email = _marie.email;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: const EdgeInsets.all(50),
        child: Column(children: [
          ListTile(
            title: const Text('Profil'),
            tileColor: Theme.of(context).primaryColorDark,
            subtitle: Text(
              'Votre profil',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Form(
              key: _formKey,
              child: Column(
                children: [
                  Row(children: [
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 3, // 30%
                        child: TextFormField(
                          validator: _textFieldValidator,
                          onChanged: (val) => _firsName = val,
                          initialValue: _firsName,
                          enabled: _updateMode,
                          maxLength: 30,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.account_circle),
                            labelText: 'First Name',
                            helperText: 'First Name',
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 4,
                        child: TextFormField(
                          initialValue: _lastName,
                          validator: _textFieldValidator,
                          onChanged: (val) => _lastName = val,
                          enabled: _updateMode,
                          maxLength: 30,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.account_circle),
                            labelText: 'Last Name',
                            helperText: 'Last Name',
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                  ]),
                  Row(children: [
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 4,
                        child: TextFormField(
                          initialValue: _email,
                          validator: _textFieldValidator,
                          onChanged: (val) => _email = val,
                          enabled: _updateMode,
                          maxLength: 30,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.contact_mail),
                            labelText: 'email',
                            helperText: 'email',
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                  ]),
                  UpdateActionBar(
                      editAction: _editAction,
                      closeAction: _closeAction,
                      doneAction: _doneAction)
                ],
              ))
        ]));
  }

  void _editAction() {
    setState(() {
      _updateMode = !_updateMode;
    });
  }

  void _closeAction() {
    setState(() {
      _formKey.currentState.reset();
      _updateMode = !_updateMode;
    });
  }

  void _doneAction() {
    final marie = Marie(
        uuid: _marie.uuid,
        firstName: _firsName,
        lastName: _lastName,
        email: _email);
    _graphqlMarieMutation.mutationMarie(marie).then((value) =>
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(seconds: 3),
            content: Text(value.message))));
    setState(() {
      _updateMode = !_updateMode;
    });
  }

  String _textFieldValidator(dynamic value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}
