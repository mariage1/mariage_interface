import 'package:flutter/material.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_invite_mutation.dart';
import 'package:mariage_ui/ui/widgets/forms/update_action_bar.dart';

class ProfilInviteCard extends StatefulWidget {
  const ProfilInviteCard({Key key, @required this.invite}) : super(key: key);

  final Invite invite;

  @override
  State<StatefulWidget> createState() => _ProfilInviteCardState();
}

class _ProfilInviteCardState extends State<ProfilInviteCard> {

  final _formKey = GlobalKey<FormState>();
  GraphqlInviteMutation _graphqlInviteMutation;

  Invite _invite;

  bool _updateMode;
  bool _presence;

  dynamic actionOnChange;

  String _firsName;
  String _lastName;
  String _email;


  @override
  void initState() {

    _graphqlInviteMutation = GraphqlInviteMutation.instance;

    _updateMode = false;
    actionOnChange = null;

    _invite = widget.invite;
    _firsName = _invite.firstName;
    _lastName = _invite.lastName;
    _email = _invite.email;
    _presence = _invite.hasValidate;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: const EdgeInsets.all(50),
        child: Column(children: [
          ListTile(
            title: const Text('Profil'),
            tileColor: Theme.of(context).primaryColorDark,
            subtitle: Text(
              'Votre profil utillisateur',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Form(
              key: _formKey,
              child: Column(
                children: [
                  Row(children: [
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 3, // 30%
                        child: TextFormField(
                          validator: _textFieldValidator,
                          onChanged: (val) => _firsName = val,
                          initialValue: _firsName,
                          enabled: _updateMode,
                          maxLength: 30,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.account_circle),
                            labelText: 'First Name',
                            helperText: 'First Name',
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 4,
                        child: TextFormField(
                          initialValue: _lastName,
                          validator: _textFieldValidator,
                          onChanged: (val) => _lastName = val,
                          enabled: _updateMode,
                          maxLength: 30,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.account_circle),
                            labelText: 'Last Name',
                            helperText: 'Last Name',
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                  ]),
                  Row(children: [
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 4,
                        child: TextFormField(
                          initialValue: _email,
                          validator: _textFieldValidator,
                          onChanged: (val) => _email = val,
                          enabled: _updateMode,
                          maxLength: 30,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.contact_mail),
                            labelText: 'email',
                            helperText: 'email',
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                    Expanded(
                        flex: 3,
                        child: DropdownButton(
                          value: _presence,
                          items: const [
                            DropdownMenuItem(
                              value: true,
                              child: Text('Je viens'),
                            ),
                            DropdownMenuItem(
                              value: false,
                              child: Text('Je ne viens pas'),
                            ),
                          ],
                          onChanged: actionOnChange,
                        )),
                    Expanded(
                      child: Container(),
                    ),
                  ]),
                  UpdateActionBar(editAction: _editAction, closeAction: _closeAction, doneAction: _doneAction)
                ],
              ))
        ]));
  }

  void _editAction() {
    setState(() {
      actionOnChange = _onChangedAction;
      _updateMode = !_updateMode;
    });
  }

  void _closeAction() {
    setState(() {
      actionOnChange = null;
      _formKey.currentState.reset();
      _updateMode = !_updateMode;
    });
  }

  void _doneAction() {
    final invite = Invite(
        uuid: _invite.uuid,
        firstName: _firsName,
        hasValidate: _presence,
        lastName: _lastName,
        email: _email);
    _graphqlInviteMutation.mutationInvite(invite, null).then((value) =>
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(seconds: 3),
            content: Text(value.message))));
    setState(() {
      actionOnChange = null;
      _updateMode = !_updateMode;
    });
  }

  void _onChangedAction(bool value) {
    setState(() {
      _presence = value;
    });
  }

  String _textFieldValidator(dynamic value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}
