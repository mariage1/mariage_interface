import 'package:flutter/material.dart';

class UpdateActionBar extends StatefulWidget {
  const UpdateActionBar({Key key,
    this.editAction,
    @required this.closeAction,
    @required this.doneAction
  }) : super(key: key);

  final Function editAction;
  final Function closeAction;
  final Function doneAction;

  @override
  _UpdateActionBarState createState() => _UpdateActionBarState();
}

class _UpdateActionBarState extends State<UpdateActionBar> {

  IconButton _editIconButton;
  IconButton _annulIconButton;
  IconButton _validIconButton;

  List<Widget> _actions;
  MainAxisAlignment _actionAlignment;

  @override
  void initState() {
    _editIconButton = IconButton(
      icon: const Icon(Icons.edit_outlined),
      onPressed: _editAction,
    );

    _annulIconButton = IconButton(
      icon: const Icon(Icons.close),
      onPressed: _closeAction,
    );

    _validIconButton = IconButton(
      icon: const Icon(Icons.done),
      onPressed: _doneAction,
    );

    _actions = [_editIconButton];
    _actionAlignment = MainAxisAlignment.end;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ButtonBar(
      alignment: _actionAlignment,
      children: _actions,
    );
  }


  void _editAction() {
    setState(() {
      widget.editAction();
      _actionAlignment = MainAxisAlignment.center;
      _actions = [_annulIconButton, _validIconButton];
    });
  }

  void _closeAction() {
    setState(() {
      widget.closeAction();
      _actionAlignment = MainAxisAlignment.end;
      _actions = [_editIconButton];
    });
  }

  void _doneAction() {
    setState(() {
      widget.doneAction();
      _actionAlignment = MainAxisAlignment.end;
      _actions = [_editIconButton];
    });
  }


}
