import 'package:flutter/material.dart';
import 'package:mariage_ui/const/colors.dart';
import 'package:mariage_ui/data/model_users/famille.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_invite_mutation.dart';
import 'package:mariage_ui/store/invites/invites_store.dart';
import 'package:pedantic/pedantic.dart';
import 'package:provider/provider.dart';

class FormInvite extends StatefulWidget {
  const FormInvite({Key key, this.invite, @required this.newInvite})
      : super(key: key);

  final Invite invite;
  final bool newInvite;

  @override
  _FormInviteStat createState() => _FormInviteStat();
}

class _FormInviteStat extends State<FormInvite> {
  final _formKey = GlobalKey<FormState>();

  bool _presence;
  String _firsName;
  String _lastName;
  String _email;

  String _dropdownCategorieValue;
  String _dropdownImportanceValue;
  String _uuid;

  String _famille;
  String _dropdownFamilleAction;

  GraphqlInviteMutation _graphqlMutationInvite;
  InvitesStore _invitesStore;
  dynamic _actionFamille;

  bool _newInvite;

  String _stats;

  @override
  void initState() {
    _newInvite = widget.newInvite;
    _newInvite ? _uuid = null : _uuid = widget.invite.uuid;
    _newInvite ? _famille = null : _famille = widget.invite.famille.name;
    _newInvite ? _presence = false : _presence = widget.invite.hasValidate;
    _firsName = widget.invite.firstName;
    _lastName = widget.invite.lastName;
    _email = widget.invite.email;

    _newInvite
        ? _dropdownCategorieValue = 'Autre'
        : _dropdownCategorieValue = widget.invite.categorie;
    _newInvite
        ? _dropdownImportanceValue = 'F'
        : _dropdownImportanceValue = widget.invite.importance;

    _dropdownFamilleAction = 'Update';
    _graphqlMutationInvite = GraphqlInviteMutation.instance;
    _invitesStore = Provider.of<InvitesStore>(context, listen: false);

    _newInvite ? _stats = 'Create' : _stats = 'Update';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: _formComponentsBuilder(),
        ));
  }

  List<Widget> _formComponentsBuilder() {
    final formComponents = <Widget>[];

    Widget _familleRow;

    final Widget _familleTextFormField = TextFormField(
        initialValue: _famille,
        maxLength: 30,
        decoration: const InputDecoration(
          icon: Icon(Icons.family_restroom),
          labelText: 'Famille',
          helperText: 'Famille',
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: primaryColorDark),
          ),
        ),
        validator: _textFieldValidator,
        onChanged: (value) {
          _famille = value;
          setState(() {
            _actionFamille = (String val) => setState(() {
                  _dropdownFamilleAction = val;
                });
          });
        });

    if (!_newInvite) {
      _familleRow = Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
            child: _familleTextFormField,
          ),
          Flexible(
              child: DropdownButton(
            value: _dropdownFamilleAction,
            items: const [
              DropdownMenuItem(
                value: 'Update',
                child: Text('Update'),
              ),
              DropdownMenuItem(
                value: 'New',
                child: Text('New Famille'),
              ),
              DropdownMenuItem(
                value: 'Switch',
                child: Text('Switch'),
              ),
            ],
            onChanged: _actionFamille,
          )),
        ],
      );
      formComponents.add(TextFormField(
        initialValue: _uuid,
        enabled: false,
        decoration: const InputDecoration(
          labelText: 'uuid',
          helperText: 'uuid',
        ),
      ));
    } else {
      _familleRow = _familleTextFormField;
    }

    formComponents.addAll([
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
              child: TextFormField(
            initialValue: _firsName,
            validator: _textFieldValidator,
            maxLength: 30,
            decoration: const InputDecoration(
              icon: Icon(Icons.account_circle),
              labelText: 'First Name',
              helperText: 'First Name',
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: primaryColorDark),
              ),
            ),
            onChanged: (val) => _firsName = val,
          )),
          Flexible(
              child: TextFormField(
            validator: _textFieldValidator,
            initialValue: _lastName,
            maxLength: 30,
            decoration: const InputDecoration(
              icon: Icon(Icons.account_circle),
              labelText: 'Last Name',
              helperText: 'Last Name',
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: primaryColorDark),
              ),
            ),
            onChanged: (value) => _lastName = value,
          )),
        ],
      ),
      TextFormField(
        initialValue: _email,
        validator: _textFieldValidator,
        decoration: const InputDecoration(
          icon: Icon(Icons.contact_mail),
          labelText: 'email',
          helperText: 'email',
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: primaryColorDark),
          ),
        ),
        onChanged: (value) {
          _email = value;
        },
      ),
      _familleRow,
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        DropdownButton(
          value: _dropdownCategorieValue,
          items: const [
            DropdownMenuItem(
              value: 'Famille',
              child: Text('Famille'),
            ),
            DropdownMenuItem(
              value: 'Amis',
              child: Text('Amis'),
            ),
            DropdownMenuItem(
              value: 'Autre',
              child: Text('Autre'),
            ),
          ],
          onChanged: (value) {
            setState(() {
              _dropdownCategorieValue = value;
            });
          },
        ),
        DropdownButton(
          value: _dropdownImportanceValue,
          items: const [
            DropdownMenuItem(
              value: 'TI',
              child: Text('Tres Important'),
            ),
            DropdownMenuItem(
              value: 'I',
              child: Text('Important'),
            ),
            DropdownMenuItem(
              value: 'F',
              child: Text('Facultatif'),
            ),
          ],
          onChanged: (value) {
            setState(() {
              _dropdownImportanceValue = value;
            });
          },
        )
      ]),
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        const Text('Presence'),
        Switch(
          value: _presence,
          onChanged: (bool value) {
            setState(() {
              _presence = value;
            });
          },
        ),
      ]),
      Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: ElevatedButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  final invite = Invite(
                      uuid: _uuid,
                      firstName: _firsName,
                      hasValidate: _presence,
                      lastName: _lastName,
                      email: _email,
                      famille: Famille(name: _famille),
                      categorie: _dropdownCategorieValue,
                      importance: _dropdownImportanceValue);
                  unawaited(_graphqlMutationInvite
                      .mutationInvite(invite, _dropdownFamilleAction)
                      .then((value) => ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(
                              duration: const Duration(seconds: 3),
                              content: Text(value.message)))));
                  unawaited(_invitesStore.loadingInvites());
                  Navigator.of(context).pop();
                }
              },
              child: Text(_stats)))
    ]);

    return formComponents;
  }

  String _textFieldValidator(dynamic value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}
