import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/data/response/socket_response.dart';
import 'package:mariage_ui/service/logger_service.dart';
import 'package:mariage_ui/service/webSocket/web_socket_activate_helper.dart';
import 'package:mariage_ui/store/user/user_store.dart';
import 'package:mariage_ui/ui/pages/login_page.dart';
import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';
import 'package:provider/provider.dart';

class ActivateUserPage extends StatefulWidget {
  const ActivateUserPage({Key key, @required this.uuid}) : super(key: key);
  static const String routeName = '/activate/';

  final String uuid;

  @override
  _ActivateUserPageState createState() => _ActivateUserPageState();
}

class _ActivateUserPageState extends State<ActivateUserPage> {
  @override
  void initState() {
    _uuid = widget.uuid;
    _userStore = Provider.of<UserStore>(context, listen: false);
    _webSocketActivateHelper = WebSocketActivateHelper.instance;
    _webSocketActivateHelper.initConnection(_getWSMessage);
    super.initState();
  }

  String _uuid;
  UserStore _userStore;
  WebSocketActivateHelper _webSocketActivateHelper;

  @override
  Widget build(BuildContext context) {
    _webSocketActivateHelper.sendJson({'uuid': _uuid});
    Widget _widget;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mariage App'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Observer(builder: (_) {
          LoggerService.instance.debug(_userStore.dataUserActivate.value.stat);
          if (_userStore.dataUserActivate.value.stat == 'Email') {
            _widget = Center(
                child: Card(
                    margin: const EdgeInsets.all(100),
                    child: Column(children: [
                      ListTile(
                        tileColor: Theme.of(context).primaryColorDark,
                        title: const Text('Validate Email'),
                      ),
                      const SizedBox(height: 100),
                      const Text(
                          'Pouvons nous utiliser cette email pour vous identifier',
                          style: TextStyle(fontSize: 30)),
                      const SizedBox(height: 75),
                      Text(
                        _userStore.dataUserActivate.value.message,
                        style: const TextStyle(fontSize: 60),
                      ),
                      const SizedBox(height: 30),
                      ButtonBar(
                        alignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            color: Colors.red,
                            focusColor: Colors.redAccent,
                            iconSize: 30,
                            icon: const Icon(Icons.close),
                            onPressed: _close,
                          ),
                          const SizedBox(width: 100),
                          IconButton(
                            icon: const Icon(Icons.done),
                            onPressed: _validate,
                          )
                        ],
                      )
                    ])));
          } else if (_userStore.dataUserActivate.value.stat == 'init') {
            _widget = const Center(child: LoadingGifAnimation());
          } else {
            _widget = Center(
                child: Card(
                    margin: const EdgeInsets.all(100),
                    child: Column(children: [
                      ListTile(
                        tileColor: Theme.of(context).primaryColorDark,
                        title: const Text('Validate Email'),
                      ),
                      const SizedBox(height: 200),
                      Text(
                        _userStore.dataUserActivate.value.message,
                        style: const TextStyle(fontSize: 60),
                      ),
                      const SizedBox(height: 30),
                      ButtonBar(
                        alignment: MainAxisAlignment.center,
                        children: [
                          TextButton.icon(
                            onPressed: () {
                              Navigator.pushNamedAndRemoveUntil(
                                  context, LoginPage.routeName, (r) => false);
                            },
                            icon: const Icon(Icons.arrow_back, size: 18),
                            label: const Text("Retour a l'application"),
                          )
                        ],
                      )
                    ])));
          }
          return _widget;
        }),
      ),
    );
  }

  void _getWSMessage(String response) {
    final Map<String, dynamic> messageJson = jsonDecode(response);
    _userStore.updateData(SocketResponse.fromJson(messageJson));
  }

  void _validate() {
    _webSocketActivateHelper.sendJson({'value': true});
  }

  void _close() {
    _webSocketActivateHelper.sendJson({'value': false});
  }
}
