import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mariage_ui/const/colors.dart';
import 'package:mariage_ui/data/security/roles.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mariage_ui/service/logger_service.dart';
import 'package:mariage_ui/store/auth/auth_store.dart';
import 'package:mariage_ui/ui/pages/profil_page.dart';
import 'package:mariage_ui/ui/pages/invites_list_page.dart';
import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);
  static const String routeName = '/login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  AuthStore _authStore;
  LoggerService _loggerService;
  final _formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  AuthService _authService;

  @override
  void initState() {
    _authService = AuthService.instance;
    _authStore = Provider.of<AuthStore>(context, listen: false);
    _loggerService = LoggerService.instance;
    _authService
        .isAuth()
        .then((value) => {
              if (value)
                {
                  if (Navigator.canPop(context))
                    {
                      _loggerService.debug('User is auth, pop context'),
                      Navigator.pop(context)
                    }
                  else
                    {_goNextPage()}
                }
            })
        .catchError((error) {
      _loggerService.error('Error dans le check de isAuth');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _widgetDisplayed;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Mariage App'),
        ),
        body: Column(
          children: [
            Observer(builder: (_) {
              switch (_authStore.isAuth.status) {
                case FutureStatus.pending:
                  _loggerService.debug('pedding');
                  _widgetDisplayed = const Center(
                    child: LoadingGifAnimation(),
                  );
                  break;
                case FutureStatus.rejected:
                  _loggerService.error('Auth futur is rejected');
                  _authService.logout().then((value) =>
                      Navigator.popAndPushNamed(context, LoginPage.routeName));
                  _widgetDisplayed = const Center(
                    child: Text('Tu es null'),
                  );
                  break;
                case FutureStatus.fulfilled:
                  _loggerService.debug('Future is sucess');
                  _widgetDisplayed = Card(
                      margin: const EdgeInsets.fromLTRB(100, 150, 100, 0),
                      child: Center(
                          child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const SizedBox(height: 50),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2, // 20%
                                        child: Container(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6, // 60%
                                        child: TextFormField(
                                          initialValue: _email,
                                          validator: _textFieldValidator,
                                          decoration: const InputDecoration(
                                            icon: Icon(Icons.contact_mail),
                                            labelText: 'email',
                                            helperText: 'email',
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: primaryColorDark),
                                            ),
                                          ),
                                          onChanged: (value) {
                                            _email = value;
                                          },
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2, // 20%
                                        child: Container(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2, // 20%
                                        child: Container(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6, // 60%
                                        child: TextFormField(
                                          initialValue: _password,
                                          validator: _textFieldValidator,
                                          obscureText: true,
                                          decoration: const InputDecoration(
                                            icon: Icon(Icons.lock_outlined),
                                            labelText: 'password',
                                            helperText: 'password',
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: primaryColorDark),
                                            ),
                                          ),
                                          onChanged: (value) {
                                            _password = value;
                                          },
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2, // 20%
                                        child: Container(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 25),
                                  Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 16),
                                      child: ElevatedButton(
                                          onPressed: () async {
                                            if (_formKey.currentState
                                                .validate()) {
                                              await _authStore
                                                  .login(_email, _password)
                                                  .then((value) {
                                                _loggerService
                                                    .debug(value.message);
                                                if (value.stat) {
                                                  _goNextPage();
                                                } else {
                                                  _loggerService
                                                      .debug(value.message);
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(SnackBar(
                                                          duration:
                                                              const Duration(
                                                                  seconds: 3),
                                                          content: Text(
                                                              value.message)));
                                                }
                                              });
                                            }
                                          },
                                          child: const Text('validate'))),
                                  const SizedBox(height: 100),
                                ],
                              ))));
                  break;
              }
              return _widgetDisplayed;
            }),
          ],
        ));
  }

  Future<void> _goNextPage() async {
    String firstRoute;
    switch (_authService.role) {
      case Roles.INVITES:
        firstRoute = ProfilePage.routeName;
        break;
      case Roles.MARIES:
      case Roles.ADMIN:
        firstRoute = InvitesListPage.routeName;
        break;
    }
    await Navigator.popAndPushNamed(context, firstRoute);
  }

  String _textFieldValidator(dynamic value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}
