import 'package:flutter/material.dart';

class ForbiddenPage extends StatelessWidget {
  const ForbiddenPage({Key key}) : super(key: key);
  static const String routeName = '/forbidden';

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body:  Center(child: Card(
          child: Text('403 Forbidden'),
        )));
  }
}
