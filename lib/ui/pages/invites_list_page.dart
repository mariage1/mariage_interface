import 'dart:html' as html;

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/data/security/roles.dart';
import 'package:mariage_ui/service/action_button_service.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mariage_ui/service/file_service.dart';
import 'package:mariage_ui/service/logger_service.dart';
import 'package:mariage_ui/service/rest/request_create_invite.dart';
import 'package:mariage_ui/store/invites/invites_store.dart';
import 'package:mariage_ui/ui/pages/generic/forbidden_page.dart';
import 'package:mariage_ui/ui/widgets/dialogs/api_reponse_alert_dialog.dart';
import 'package:mariage_ui/ui/widgets/dialogs/download_invitations_dialog.dart';
import 'package:mariage_ui/ui/widgets/dialogs/invite_update_dialoge.dart';
import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';
import 'package:mariage_ui/ui/widgets/tables/invites_table/invites_table.dart';
import 'package:mobx/mobx.dart';
import 'package:pedantic/pedantic.dart';
import 'package:provider/provider.dart';

class InvitesListPage extends StatefulWidget {
  const InvitesListPage({Key key}) : super(key: key);
  static const String routeName = '/invites-list-page';

  @override
  _InvitesListPageState createState() => _InvitesListPageState();
}

class _InvitesListPageState extends State<InvitesListPage> {
  InvitesStore _invitesStore;
  FileService _fileService;
  AuthService _authService;
  LoggerService _loggerService;
  ActionButtonService _actionButtonService;
  QRCodeHelper _codeHelper;
  Icon _pending;
  IconButton _actionDolow;

  @override
  void initState() {
    _loggerService = LoggerService.instance;
    _authService = AuthService.instance;
    _invitesStore = Provider.of<InvitesStore>(context, listen: false);
    _invitesStore.loadingInvites();
    _fileService = FileService.instance;
    _actionButtonService = ActionButtonService.instance;
    _codeHelper = QRCodeHelper.instance;
    _pending = const Icon(
      Icons.qr_code_outlined,
    );
    _actionDolow = IconButton(
        tooltip: 'Télécharger les invitations',
        icon: _pending,
        onPressed: _alertDownloadInvitations);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _widgetFutureDisplayed;
    Widget _widgetDisplayed;
    _actionButtonService.initAction(context, InvitesListPage.routeName);
    switch (_authService.role) {
      case Roles.MARIES:
      case Roles.ADMIN:
        _widgetDisplayed = Scaffold(
            appBar: AppBar(
              title: const Text('Mariage App'),
              actions: _actionButtonService.actions,
            ),
            body: Observer(
              builder: (_) {
                switch (_invitesStore.listInvites.status) {
                  case FutureStatus.pending:
                    _widgetFutureDisplayed = const Center(
                      child: LoadingGifAnimation(),
                    );
                    break;
                  case FutureStatus.rejected:
                    _loggerService.error('Future auth is rejected');
                    _authService.logout().then((value) =>
                        Navigator.canPop(context)
                            ? Navigator.pop(context)
                            : _loggerService.error('pop is not posibel'));
                    _widgetFutureDisplayed = const Text('erreur');
                    break;
                  case FutureStatus.fulfilled:
                    _widgetFutureDisplayed = Center(
                      child: ListView(
                          padding: const EdgeInsets.all(16),
                          children: [
                            InvitesTable(
                                invites:
                                    _invitesStore.listInvites.value.invites),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: const Icon(Icons.note_add),
                                  tooltip:
                                      'Ajouter des invites via fichier csv',
                                  onPressed: () async {
                                    await _postFile();
                                    unawaited(_invitesStore.loadingInvites());
                                  },
                                  splashRadius: 20,
                                ),
                                IconButton(
                                  icon: const Icon(Icons.person_add),
                                  tooltip: 'Ajouter un inviter',
                                  onPressed: _addInvite,
                                  splashRadius: 20,
                                ),
                                _actionDolow
                              ],
                            )
                          ]),
                    );
                    break;
                }
                return _widgetFutureDisplayed;
              },
            ));
        break;
      case Roles.INVITES:
      default:
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        _loggerService.warning('Role non adapter pour cette page');
        _widgetDisplayed = const ForbiddenPage();
        break;
    }
    return _widgetDisplayed;
  }

  Future<void> _postFile() async {
    final res = _fileService.uploadFile();
    await _showSendCSVDialog(res);
  }

  Future<void> _addInvite() async {
    await showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return const InviteFormDialogue(
            newInvite: true,
          );
        });
  }

  Future<void> _alertDownloadInvitations() async {

    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return DownloadInvitationsDialog(
            downloadFunction: _downloadInvitations,
          );
        });
  }

  Future<void> _downloadInvitations(bool generat) async {
    final blob = await _codeHelper.getInvitation(generat);
    final url = html.Url.createObjectUrlFromBlob(blob);
    LoggerService.instance.debug(url);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = 'invitation.pdf';
    html.document.body.children.add(anchor);
    anchor.click();
    html.document.body.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
  }

  Future<void> _showSendCSVDialog(Future<ApiResponse> response) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return ApiResponseAlertDialog(
            response: response,
            titre: 'Add Invites by CSV',
          );
        });
  }
}
