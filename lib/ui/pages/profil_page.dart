import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/data/model_users/marie.dart';
import 'package:mariage_ui/data/security/roles.dart';
import 'package:mariage_ui/service/action_button_service.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mariage_ui/service/logger_service.dart';
import 'package:mariage_ui/store/auth/auth_store.dart';
import 'package:mariage_ui/store/user/user_store.dart';
import 'package:mariage_ui/ui/pages/generic/forbidden_page.dart';
import 'package:mariage_ui/ui/pages/generic/not_found.dart';
import 'package:mariage_ui/ui/widgets/cards/famille_card.dart';
import 'package:mariage_ui/ui/widgets/cards/profil_card/profil_invite_card.dart';
import 'package:mariage_ui/ui/widgets/cards/profil_card/profil_marie_card.dart';
import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);
  static const String routeName = '/me';

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ActionButtonService _actionButtonService;
  LoggerService _loggerService;
  AuthStore _authStore;
  UserStore _userStore;

  @override
  void initState() {
    _loggerService = LoggerService.instance;
    _actionButtonService = ActionButtonService.instance;
    _authStore = Provider.of<AuthStore>(context, listen: false);
    _userStore = Provider.of<UserStore>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _actionButtonService.initAction(context, ProfilePage.routeName);
    Widget _widgetDisplayed;
    Widget _widgetFutureDisplayed;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Mariage App'),
          actions: _actionButtonService.actions,
        ),
        body: Observer(builder: (_) {
          switch (_authStore.isAuth.status) {
            case FutureStatus.pending:
              _widgetDisplayed = const Center(
                child: LoadingGifAnimation(),
              );
              break;
            case FutureStatus.rejected:
              _loggerService.error('problem dans le check authentication');
              _widgetDisplayed = const ForbiddenPage();
              break;
            case FutureStatus.fulfilled:
              if (!_authStore.isAuth.value) {
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
                _loggerService.warning('Role non adapter pour cette page');
                _widgetDisplayed = const ForbiddenPage();
              }
              _userStore.getCurantUser();
              _widgetDisplayed = Observer(builder: (_) {
                switch (_userStore.curantUser.status) {
                  case FutureStatus.pending:
                    _widgetFutureDisplayed =
                        const Center(child: LoadingGifAnimation());
                    break;
                  case FutureStatus.rejected:
                    _loggerService
                        .error('problem dans la recuperation du user courant');
                    _widgetFutureDisplayed = const NotFoundPage();
                    break;
                  case FutureStatus.fulfilled:
                    final  _currentUser =
                        _userStore.curantUser.value;
                    switch (AuthService.instance.role) {
                      case Roles.INVITES:

                        _widgetFutureDisplayed =
                            _buildInviteProfilePage(_currentUser);
                        break;
                      case Roles.MARIES:
                        _widgetFutureDisplayed =_buildMarieProfilePage(_currentUser);
                        break;
                      case Roles.ADMIN:
                        _widgetFutureDisplayed = const Text('In progress');
                        break;
                    }
                }
                return _widgetFutureDisplayed;
              });
              break;
          }
          return _widgetDisplayed;
        }));
  }

  Widget _buildInviteProfilePage(Invite invite) {
    final widget = <Widget>[
      ProfilInviteCard(
        invite: invite,
      )
    ];
    if (invite.famille.membres.length > 1) {
      widget.add(FamilyCard(famille: invite.famille));
    }
    return ListView(children: widget);
  }


  Widget _buildMarieProfilePage(Marie marie){
    final widget = <Widget>[
      ProfilMarieCard(marie: marie)
    ];
    return ListView(children: widget);
  }

}
