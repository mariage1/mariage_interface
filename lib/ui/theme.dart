import 'package:flutter/material.dart';
import 'package:mariage_ui/const/colors.dart';

class LightTheme {
  static final ThemeData defaultTheme = _buildTheme();

  static ThemeData _buildTheme() {
    final base = ThemeData.light();

    return base.copyWith(
        primaryColorLight: primaryLightColor,
        accentColor: secondaryColor,
        accentColorBrightness: Brightness.dark,
        buttonColor: primaryLightColor,
        primaryColor: primaryColor,
        primaryColorBrightness: Brightness.dark,
        buttonTheme: base.buttonTheme.copyWith(
          buttonColor: secondaryColor,
          textTheme: ButtonTextTheme.primary,
        ),
        floatingActionButtonTheme: base.floatingActionButtonTheme
            .copyWith(backgroundColor: secondaryColor),
        textTheme: TextTheme(
          bodyText2: TextStyle(
            color: blackColor,
            fontFamily: 'Garamond',
          ),
        ),
        bottomAppBarColor: secondaryColor,
        scaffoldBackgroundColor: background,
        backgroundColor: background,
        iconTheme: base.iconTheme.copyWith(color: blackColor),
        appBarTheme: base.appBarTheme.copyWith(
            color: primaryColor,
            textTheme: TextTheme(
              headline6: TextStyle(
                color: blackColor,
                fontSize: 20,
                fontFamily: 'Garamond',
              ),
              bodyText2: TextStyle(
                color: blackColor,
                fontFamily: 'Garamond',
              ),
            ),
            elevation: 0),
        errorColor: negativeColor,
        indicatorColor: positiveColor,
        textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
          backgroundColor: primaryColorDark,
          primary: Colors.white,
          padding: EdgeInsets.all(20),
        )));
  }
}
