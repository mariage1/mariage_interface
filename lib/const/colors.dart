import 'dart:ui';

const primaryColor = Color(0xFF4FC3F7);
const primaryColorDark = Color(0xFF0093C4);
const primaryLightColor = Color(0xFF8bf6ff);

const secondaryColor = Color(0xFFFFB300);
const secondaryColorDark = Color(0xFFC68400);
const secondaryLightColor = Color(0xFFffe54c);

const background = Color(0xFFEAEAEA);
const backgroundDark = Color(0xFF212529);

const blackColor = Color(0xFF000000);
const whiteColor = Color(0xFFf8f9fa);
const transparent = Color(0x00000000);

const positiveColor = Color(0xFF00BA69);
const negativeColor = Color(0xFFFF2F2F);