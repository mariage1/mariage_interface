enum Environement { DEV, STAGING, PROD }

class Constanants {
  static Map<String, dynamic> _config;
  static void setEnvironement(Environement environement) {
    switch (environement) {
      case Environement.DEV:
        _config = _Config.devConstants;
        break;
      case Environement.PROD:
        _config = _Config.prodConstants;
        break;
      case Environement.STAGING:
        _config = _Config.stagingConstants;
        break;
    }
  }

  static dynamic get wereAmIURL => _config[_Config.WERE_AM_I];
  static bool get isDebuggable => _config[_Config.IS_DEBUGGABLE];
  static String get verboseLevel => _config[_Config.VERBOSE_LEVEL];
  static String get apiURL => _config[_Config.API_URL];
  static String get wsURL => _config[_Config.WS_URL];
}

mixin _Config {
  static const WERE_AM_I = 'WERE_AM_I';
  static const IS_DEBUGGABLE = 'IS_DEBUGGABLE';
  static const VERBOSE_LEVEL = 'VERBOSE_LEVEL';
  static const API_URL = 'API_URL';
  static const WS_URL = 'NAME_URL';

  static Map<String, dynamic> devConstants = {
    WERE_AM_I: 'DEV',
    IS_DEBUGGABLE: true,
    VERBOSE_LEVEL: 'verbose',
    API_URL: 'http://localhost:8080',
    WS_URL: 'ws://localhost:8080',
  };
  static Map<String, dynamic> stagingConstants = {
    WERE_AM_I: 'STAGING',
    IS_DEBUGGABLE: true,
    VERBOSE_LEVEL: 'info',
    API_URL: 'https://app-mariage.bastienmarais.fr',
    WS_URL: 'wss://app-mariage.bastienmarais.fr',
  };
  static Map<String, dynamic> prodConstants = {
    WERE_AM_I: 'PROD',
    IS_DEBUGGABLE: false,
    VERBOSE_LEVEL: 'info',
    API_URL: 'https://mariage-api.herokuapp.com',
    WS_URL: 'mariage-api.herokuapp.com',
  };
}
