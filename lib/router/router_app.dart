import 'package:flutter/material.dart';
import 'package:mariage_ui/ui/pages/activate_user_page.dart';
import 'package:mariage_ui/ui/pages/generic/forbidden_page.dart';
import 'package:mariage_ui/ui/pages/generic/not_found.dart';
import 'package:mariage_ui/ui/pages/invites_list_page.dart';
import 'package:mariage_ui/ui/pages/profil_page.dart';
import 'package:mariage_ui/ui/pages/login_page.dart';

class RouterApp {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ProfilePage.routeName:
        return MaterialPageRoute(builder: (_) => const ProfilePage());
      case InvitesListPage.routeName:
        return MaterialPageRoute(builder: (_) => const InvitesListPage());
      case LoginPage.routeName:
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case '/':
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case ForbiddenPage.routeName:
        return MaterialPageRoute(builder: (_) => const ForbiddenPage());
    }

    final regex = RegExp(
        r'^(\/.*\/)([0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12})$');
    final match = regex.firstMatch(settings.name);

    switch (match.group(1)) {
      case ActivateUserPage.routeName:
        return MaterialPageRoute(
            builder: (_) => ActivateUserPage(uuid: match.group(2).toString()));
    }
    return MaterialPageRoute(builder: (_) => const NotFoundPage());
  }
}
