import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mariage_ui/data/security/roles.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mariage_ui/store/auth/auth_store.dart';
import 'package:mariage_ui/ui/pages/invites_list_page.dart';
import 'package:mariage_ui/ui/pages/login_page.dart';
import 'package:mariage_ui/ui/pages/profil_page.dart';
import 'package:provider/provider.dart';

class ActionButtonService {
  ActionButtonService._privateConstructor() {
    _authService = AuthService.instance;
    _actions = [];
  }

  static final ActionButtonService _instance =
      ActionButtonService._privateConstructor();
  static ActionButtonService get instance => _instance;

  List<Widget> _actions;
  AuthService _authService;
  AuthStore _authStore;

  List<Widget> get actions => _actions;

  void initAction(BuildContext context, String currentRoute) {
    _authStore = Provider.of<AuthStore>(context, listen: false);
    _actions = [
      IconButton(
          icon: const Icon(Icons.logout),
          onPressed: () => _logoutAction(context))
    ];
    switch (_authService.role) {
      case Roles.INVITES:
        break;
      case Roles.MARIES:
        if(currentRoute != ProfilePage.routeName){
          _actions.insert(
              0,
              IconButton(
                  icon: const Icon(Icons.account_circle),
                  onPressed: () => _nextPage(context, ProfilePage.routeName)));
        }
        if(currentRoute != InvitesListPage.routeName){
          _actions.insert(
              0,
              IconButton(
                  icon: const Icon(Icons.list),
                  onPressed: () => _nextPage(context, InvitesListPage.routeName)));
        }

        break;
      case Roles.ADMIN:
        break;
    }
  }

  Future<void> _logoutAction(BuildContext context) async {
    await _authStore.logout();
    await Navigator.pushNamedAndRemoveUntil(
        context, LoginPage.routeName, (Route<dynamic> route) => false);
  }

  Future<void> _nextPage(BuildContext context, String routeName) async {
    await Navigator.pushNamed(context, routeName);
  }
}
