import 'package:mariage_ui/service/webSocket/web_socket_service.dart';

class WebSocketActivateHelper {
  WebSocketActivateHelper._privateConstructor() {
    _webSocketService = WebSocketService.instance;

  }

  static final WebSocketActivateHelper _instance =
      WebSocketActivateHelper._privateConstructor();
  static WebSocketActivateHelper get instance => _instance;
  WebSocketService _webSocketService;


  void sendJson(Map<String, dynamic> message){
    _webSocketService.sendMessage(message);
  }

  void initConnection(Function collBack) {
      _webSocketService.connectToChanel();
      _webSocketService.addListener(collBack);
  }

  void nextStep({ Function nexCollBack, Function oldCollBack = null}){
    if(oldCollBack == null){
      _webSocketService.removeListener(oldCollBack);
    }
    _webSocketService.addListener(nexCollBack);
  }
}
