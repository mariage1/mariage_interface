import 'dart:convert';

import 'package:mariage_ui/const/constants.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class WebSocketService {
  WebSocketService._privateConstructor() {
    _listeners = [];
    _onConect = false;
  }

  static final WebSocketService _instance =
      WebSocketService._privateConstructor();
  static WebSocketService get instance => _instance;

  WebSocketChannel _webSocketChannel;
  List<Function> _listeners;
  bool _onConect;

  void connectToChanel() {
    if(_onConect){
      closeConnection();
    }
    _webSocketChannel = WebSocketChannel.connect(
        Uri.parse('${Constanants.wsURL}/webSocket/activate'));
    _webSocketChannel.stream.listen(_onReceptionOfMessageFromServer);
    _onConect = true;
    return;
  }

  void closeConnection() {
    _webSocketChannel.sink.close();
    _onConect = false;
  }

  void sendMessage(Map<String, dynamic> message) {
    _webSocketChannel.sink.add(jsonEncode(message));
  }

  void addListener(Function callback) {
    _listeners.add(callback);
  }

  void removeListener(Function callback) {
    _listeners.remove(callback);
  }

  void _onReceptionOfMessageFromServer(dynamic message) {
    for (final callback in _listeners) {
      callback(message);
    }
  }
}
