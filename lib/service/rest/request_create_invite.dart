import 'dart:html' as html;
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:mariage_ui/const/constants.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mariage_ui/service/logger_service.dart';

class QRCodeHelper {
  QRCodeHelper._privateConstructor();



  static final QRCodeHelper _instance = QRCodeHelper._privateConstructor();
  static QRCodeHelper get instance => _instance;

  Future<html.Blob> getInvitation(bool generate) async {
    try {
      final headers = {
        'Authorization':
            '${AuthService.instance.tokenModel.type} ${AuthService.instance.tokenModel.token}',
      };
      final request = http.Request('GET', Uri.parse('${Constanants.apiURL}/rest/invites/invitation?generate_invitation=$generate'));
      request.headers.addAll(headers);

      final response = await request.send();
      LoggerService.instance.debug(response.headers.toString());
      return html.Blob([await response.stream.toBytes()], 'application/pdf');
    } on SocketException catch (e) {
      throw Exception(e.message);
    }
  }
}
