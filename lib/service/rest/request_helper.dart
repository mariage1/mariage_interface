import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:mariage_ui/const/constants.dart';
import 'package:mariage_ui/data/response/response.dart';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mariage_ui/service/auth_service.dart';

class RequestHelper {
  RequestHelper._privateConstructor() {
    url = Uri.parse('${Constanants.apiURL}/rest/invites/uploadfile');
  }

  Uri url;

  static final RequestHelper _instance = RequestHelper._privateConstructor();
  static RequestHelper get instance => _instance;

  Future<ApiResponse> sendFile(PlatformFile file) async {
    ApiResponse apiResponse;

    final List<int> _selectedFile = file.bytes;
    final request = http.MultipartRequest('POST', url);
    request.headers.addAll({'Authorization': '${AuthService.instance.tokenModel.type} ${AuthService.instance.tokenModel.token}'});
    request.files.add(http.MultipartFile.fromBytes('file', _selectedFile,
        contentType: MediaType('application', 'octet-stream'),
        filename: 'invites_file.csv'));
    await request.send().then((response) async {
      await response.stream.bytesToString().then(
          (value){
            final resJson = json.decode(value);
            apiResponse = ApiResponse.fromJson(resJson['detail'] != null ? resJson['detail'] : resJson);
          } );
    });
    return apiResponse;
  }
}
