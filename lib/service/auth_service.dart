import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/data/security/roles.dart';
import 'package:mariage_ui/data/security/token_model.dart';
import 'package:mariage_ui/service/graphql/mutation/graphql_login_mutation.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';

class AuthService {
  AuthService._privateConstructor() {
    _loggerService = LoggerService.instance;
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _graphqlMutationLogin = GraphqlMutationLogin.instance;
  }

  static final AuthService _instance = AuthService._privateConstructor();
  static AuthService get instance => _instance;
  TokenModel get tokenModel => _tokenModel;

  InstanceGraphQLClient _instanceGraphQLClient;
  GraphqlMutationLogin _graphqlMutationLogin;
  LoggerService _loggerService;
  TokenModel _tokenModel;
  final String _boxName = 'TokenBox';
  Roles _role;
  Box<dynamic> _box;

  Future<ApiResponse> loginService(String email, String password) async {
    _box ??= await Hive.openBox(_boxName);

    final apiLoginResponse =
        await _graphqlMutationLogin.mutationLogin(email, password);
    _loggerService.debug(apiLoginResponse.apiResponse.message);
    _tokenModel =
        TokenModel(apiLoginResponse.token, apiLoginResponse.tokenType);
    if(_tokenModel.token != null){
      await _box.put('token', jsonEncode(_tokenModel.toJson()));
      await _updateRole();
      _instanceGraphQLClient.updateToken(_tokenModel);
    }
    return apiLoginResponse.apiResponse;
  }

  Future<bool> isAuth() async {
    _loggerService.debug('Check auth');
    _box ??= await Hive.openBox(_boxName);
    final memory = _box.get('token');
    if (memory != null) {
      _tokenModel = TokenModel.fromJson(jsonDecode(memory));
      await _updateRole();
    } else {
      return false;
    }
    if (_tokenModel == null) {
      return false;
    }
    if(_tokenModel.tokenIsValide()){
      _instanceGraphQLClient.updateToken(_tokenModel);
      return true;
    }
    return false;
  }

  Future<bool> logout() async {
    _tokenModel = null;
    _role = null;
    await _box.delete('token');
    _instanceGraphQLClient.updateToken(null);
    return true;
  }

  Roles get role => _role;

  Future<void> _updateRole() async {
    final decodedToken = JwtDecoder.decode(_tokenModel.token);
    switch (decodedToken['role']) {
      case 'maries':
        _role = Roles.MARIES;
        break;
      case 'admin':
        _role = Roles.ADMIN;
        break;
      case 'invites':
      default:
        _role = Roles.INVITES;
        break;
    }
  }
}
