// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

import 'package:file_picker/file_picker.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/service/rest/request_helper.dart';

class FileService {
  FileService._privateConstructor() {
    uploadInput = FileUploadInputElement();
    dioHelper = RequestHelper.instance;
  }

  static final FileService _instance = FileService._privateConstructor();

  InputElement uploadInput;
  RequestHelper dioHelper;

  static FileService get instance => _instance;


  Future<ApiResponse> uploadFile() async {
    ApiResponse apiResponse;

    final result = await FilePicker.platform.pickFiles();

    if(result != null){
      apiResponse = await dioHelper.sendFile(result.files.single);
    }

    return apiResponse;

  }

}
