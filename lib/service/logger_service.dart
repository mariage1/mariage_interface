import 'package:logger/logger.dart';
import 'package:mariage_ui/const/constants.dart';

class LoggerService {
  LoggerService._privateConstructor() {
    switch (Constanants.verboseLevel) {
      case 'verbose':
        Logger.level = Level.verbose;
        break;
      case 'debug':
        Logger.level = Level.debug;
        break;
      case 'info':
      default:
        Logger.level = Level.info;
        break;
    }

    _logger = Logger();
  }

  static final LoggerService _instance = LoggerService._privateConstructor();

  static LoggerService get instance => _instance;

  Logger _logger;

  void debug(String message) => _logger.d(message);
  void info(String message) => _logger.i(message);
  void error(String message) => _logger.e(message);
  void warning(String message) => _logger.w(message);
}
