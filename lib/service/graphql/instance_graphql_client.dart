import 'package:graphql/client.dart';
import 'package:mariage_ui/const/constants.dart';
import 'package:mariage_ui/data/security/token_model.dart';

class InstanceGraphQLClient {
  InstanceGraphQLClient._privateConstructor() {
    _url = Constanants.apiURL;
    _hasToken = false;
    _graphQLClient ??= GraphQLClient(
      link: HttpLink('$_url/graphql'),
      cache: GraphQLCache(),
      defaultPolicies: DefaultPolicies(
        watchQuery: _policies,
        query: _policies,
        mutate: _policies,
      ),
    );
  }
  final _policies = Policies(
    fetch: FetchPolicy.networkOnly,
  );

  GraphQLClient _graphQLClient;
  String _url;

  static final InstanceGraphQLClient _instance =
  InstanceGraphQLClient._privateConstructor();

  static InstanceGraphQLClient get instance => _instance;

  GraphQLClient get graphQLClient => _graphQLClient;
  bool get hasToken => _hasToken;

  bool _hasToken;

  void updateToken(TokenModel tokenModel) {

    if (tokenModel?.token != null) {
      _hasToken = true;
    } else {
      _hasToken = false;
    }

    _graphQLClient = GraphQLClient(
      link: HttpLink('$_url/graphql', defaultHeaders: {
        'Authorization': '${tokenModel?.type} ${tokenModel?.token}'
      }),
      cache: GraphQLCache(),
      defaultPolicies: DefaultPolicies(
        watchQuery: _policies,
        query: _policies,
        mutate: _policies,
      ),
    );
  }


}