import 'package:graphql/client.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/data/model_users/marie.dart';
import 'package:mariage_ui/data/model_users/user.dart';
import 'package:mariage_ui/data/security/roles.dart';
import 'package:mariage_ui/service/auth_service.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';

class GraphqlProfilQuery {
  GraphqlProfilQuery._privateConstructor() {
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _loggerService = LoggerService.instance;
  }

  LoggerService _loggerService;
  InstanceGraphQLClient _instanceGraphQLClient;

  static final GraphqlProfilQuery _instance =
  GraphqlProfilQuery._privateConstructor();

  static GraphqlProfilQuery get instance => _instance;

  Future<User> me() async {
    User me;

    if(!_instanceGraphQLClient.hasToken){
      _loggerService.error('Token required for this query');
      throw Exception('Token required for this query');
    }

    final options = QueryOptions(document: gql('''
        query me{
  curantUser{
    __typename,
            ... on MarieSchema{
            uuid
            lastName
            firstName
            email
            }
          ... on InviteSchema{
            uuid
            firstName
            lastName
            email
            hasValidate
            famille{
            uuid
              name
              membres {
                uuid
                firstName
                lastName
                hasValidate
              }
            }
        }      
    }
}
    '''));

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }
    final curantUser = result.data['curantUser'];
    switch(AuthService.instance.role){
      case Roles.INVITES:
        if(curantUser['__typename'] != 'InviteSchema'){
          _loggerService.error("Erreur dans le role du user renvoyé par l'API");
          throw Exception('Error on role');
        }
        me = Invite.fromJson(curantUser);
        break;
      case Roles.MARIES:
        if(curantUser['__typename'] != 'MarieSchema'){
          _loggerService.error("Erreur dans le role du user renvoyé par l'API");
          throw Exception('Error on role');
        }
        me = Marie.fromJson(curantUser);
        break;
      case Roles.ADMIN:
        _loggerService.warning('Roles Admin non implementé');
        break;
    }
    return me;
  }


}