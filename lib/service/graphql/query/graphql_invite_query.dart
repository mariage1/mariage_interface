import 'package:graphql/client.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/data/model_users/list_invites.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';

class GraphqlInviteQuery {
  GraphqlInviteQuery._privateConstructor() {
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _loggerService = LoggerService.instance;
  }

  LoggerService _loggerService;
  InstanceGraphQLClient _instanceGraphQLClient;

  static final GraphqlInviteQuery _instance =
      GraphqlInviteQuery._privateConstructor();

  static GraphqlInviteQuery get instance => _instance;


  Future<ListInvites> queryInvitesInfo() async {
    if(!_instanceGraphQLClient.hasToken){
      _loggerService.error('Token required for this query');
      throw Exception('Token required for this query');
    }

    final options = QueryOptions(
      // ignore: unnecessary_raw_strings
      document: gql(r'''
        query {
          invites {
            uuid
            firstName
            lastName
            categorie
            hasValidate
            importance
            email
            famille{
              name
            }
          }        
        }
        '''),
    );

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }

    return ListInvites.fromJson(result.data);
  }

  Future<Invite> queryInvite(String uuid) async {
    final options = QueryOptions(
      document: gql(r'''
          query getInvite($uuid: String){
          invite(inviteUuid: $uuid ){
                uuid
                firstName
                lastName
                email
                categorie
                hasValidate
                importance
                marie{
                  firstName
                }
                famille{
                  name
                }
              }
          }
        '''),
      variables: <String, dynamic>{
        'uuid': uuid,
      },
    );

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }

    return  Invite.fromJson(result.data['invite']);
  }
}
