import 'package:graphql/client.dart';
import 'package:mariage_ui/data/model_users/marie.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';

class GraphqlMarieMutation {
  GraphqlMarieMutation._privateConstructor() {
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _loggerService = LoggerService.instance;
  }

  LoggerService _loggerService;
  InstanceGraphQLClient _instanceGraphQLClient;

  static final GraphqlMarieMutation _instance =
  GraphqlMarieMutation._privateConstructor();

  static GraphqlMarieMutation get instance => _instance;


  Future<ApiResponse> mutationMarie(Marie marie) async {
    final variables = <String, dynamic>{'marie': marie.toJson()};
    final options = QueryOptions(
      document: gql(r'''
         mutation mutationMarie($marie: MarieInputSchema!){
          mutationMarie(marieData: $marie){
            stats
            message
          }
        }
        '''),
      variables: variables ,
    );

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }
    return ApiResponse.fromJson(result.data['mutationMarie']);
  }

}