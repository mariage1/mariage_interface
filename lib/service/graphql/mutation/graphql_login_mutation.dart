import 'package:graphql/client.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';

class GraphqlMutationLogin {
  GraphqlMutationLogin._privateConstructor() {
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _loggerService = LoggerService.instance;
  }

  LoggerService _loggerService;
  InstanceGraphQLClient _instanceGraphQLClient;

  static final GraphqlMutationLogin _instance =
  GraphqlMutationLogin._privateConstructor();

  static GraphqlMutationLogin get instance => _instance;

  Future<ApiLoginResponse> mutationLogin(String email, String password) async {
    final options = QueryOptions(document: gql(r'''
    mutation login($loginInfo: LoginInputSchema!){
      mutationLogin(loginInfo: $loginInfo){
        stats
        message
        token
        tokenType
      }
    }
    '''), variables: <String, dynamic>{
      'loginInfo': {'email': email, 'password': password}
    });

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }

    return ApiLoginResponse.fromJson(result.data['mutationLogin']);
  }


}