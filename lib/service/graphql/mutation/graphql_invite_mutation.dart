import 'package:graphql/client.dart';
import 'package:mariage_ui/data/model_users/invites.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';

class GraphqlInviteMutation {
  GraphqlInviteMutation._privateConstructor() {
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _loggerService = LoggerService.instance;
  }

  LoggerService _loggerService;
  InstanceGraphQLClient _instanceGraphQLClient;

  static final GraphqlInviteMutation _instance =
      GraphqlInviteMutation._privateConstructor();

  static GraphqlInviteMutation get instance => _instance;

  Future<ApiResponse> mutationInvite(Invite invite,String action) async {
    final options = QueryOptions(
      document: gql(r'''
         mutation mutationInvite($invite: InviteUpdateSchema!, $action: String){
          mutationInvite(inviteData: $invite, familleAction: $action){
            stats
            message
          }
        }
        '''),
      variables: <String, dynamic>{'invite': invite.toJson(), 'action': action},
    );

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }

    return  ApiResponse.fromJson(result.data['mutationInvite']);
  }

  Future<ApiResponse> mutationDeleteInvite(List<String> uuids) async {
    final options = QueryOptions(document: gql(r'''
        mutation mutationDeleteInvite($uuid: [UUID]!){
          deleteInvite(invitesUuids: $uuid){
            stats
            message
          }
        }
        '''), variables: <String, dynamic>{'uuid': uuids});

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }

    return  ApiResponse.fromJson(result.data['deleteInvite']);
  }
}
