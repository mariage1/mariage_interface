import 'package:graphql/client.dart';
import 'package:mariage_ui/data/model_users/famille.dart';
import 'package:mariage_ui/data/response/response.dart';
import 'package:mariage_ui/service/graphql/instance_graphql_client.dart';
import 'package:mariage_ui/service/logger_service.dart';



class GraphqlFamilleMutation {
  GraphqlFamilleMutation._privateConstructor() {
    _instanceGraphQLClient = InstanceGraphQLClient.instance;
    _loggerService = LoggerService.instance;
  }

  LoggerService _loggerService;
  InstanceGraphQLClient _instanceGraphQLClient;

  static final GraphqlFamilleMutation _instance =
  GraphqlFamilleMutation._privateConstructor();

  static GraphqlFamilleMutation get instance => _instance;


  Future<ApiResponse> mutationFamille(Famille famille) async {
    final variables = <String, dynamic>{'famille': famille.toJson(removeMembres: true) };
    final options = QueryOptions(
      document: gql(r'''
         mutation mutationFamille($famille: FamilleInputSchema!){
          mutationFamille(familleData: $famille){
            stats
            message
          }
        }
        '''),
      variables: variables ,
    );

    final result = await _instanceGraphQLClient.graphQLClient.query(options);

    if (result.hasException) {
      _loggerService.error(result.exception.toString());
      throw Exception(result.exception.toString());
    }

    return  ApiResponse.fromJson(result.data['mutationFamille']);
  }

}