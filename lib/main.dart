import 'package:flutter/material.dart';
import 'package:mariage_ui/router/router_app.dart';
import 'package:mariage_ui/store/auth/auth_store.dart';
import 'package:mariage_ui/store/invites/invites_store.dart';
import 'package:mariage_ui/store/user/user_store.dart';
import 'package:mariage_ui/ui/theme.dart';
import 'package:mariage_ui/ui/widgets/loading/loading_gif_animation.dart';
import 'package:provider/provider.dart';

import 'const/constants.dart';

void main() {
  const env = String.fromEnvironment('ENV', defaultValue: 'dev');
  switch (env) {
    case 'dev':
      Constanants.setEnvironement(Environement.DEV);
      break;
    case 'staging':
      Constanants.setEnvironement(Environement.STAGING);
      break;
    case 'prod':
      Constanants.setEnvironement(Environement.PROD);
      break;
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key key}) : super(key: key);

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InvitesStore _invitesStore;
  AuthStore _authStore;
  UserStore _userStore;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initApp(context),
        builder: (context, snapshot) {
          if (snapshot.hasError ||
              (snapshot.connectionState == ConnectionState.done)) {
            return MultiProvider(
              providers: [
                Provider<InvitesStore>(
                  create: (_) => _invitesStore,
                ),
                Provider<AuthStore>(create: (_) => _authStore),
                Provider<UserStore>(create: (_) => _userStore)
              ],
              child: MaterialApp(
                title: 'Flutter Demo',
                theme: LightTheme.defaultTheme,
                onGenerateRoute: RouterApp.generateRoute,
                navigatorKey: navigatorKey,
              ),
            );
          }
          return MaterialApp(
              title: 'LoadingScreen',
              theme: LightTheme.defaultTheme,
              home: const Scaffold(
                body: Center(
                  child: LoadingGifAnimation(),
                ),
              ));
        });
  }

  Future<void> _initApp(context) async {
    _invitesStore = InvitesStore();
    _authStore = AuthStore();
    _userStore = UserStore();
  }
}
