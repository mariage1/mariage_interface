import 'package:mariage_ui/data/model_users/famille.dart';
import 'package:mariage_ui/data/model_users/marie.dart';
import 'package:mariage_ui/data/model_users/user.dart';

class Invite extends User {
  Invite(
      {String uuid,
      String firstName,
      String lastName,
      String email,
      DateTime datetimeCreateUser,
      DateTime datetimeLastConnection,
      this.hasValidate,
      this.categorie,
      this.importance,
      this.marie,
      this.famille})
      : super(
            uuid: uuid,
            firstName: firstName,
            lastName: lastName,
            email: email,
            datetimeCreateUser: datetimeCreateUser,
            datetimeLastConnection: datetimeLastConnection);

  Invite.fromJson(Map<String, dynamic> json) {
    if (json['uuid'] != null) {
      uuid = json['uuid'];
    } else {
      uuid = null;
    }
    if (json['firstName'] != null) {
      firstName = json['firstName'];
    } else {
      firstName = null;
    }
    if (json['lastName'] != null) {
      lastName = json['lastName'];
    } else {
      lastName = null;
    }
    if (json['email'] != null) {
      email = json['email'];
    } else {
      email = null;
    }
    datetimeCreateUser = json['datetimeCreateUser'] != null
        ? DateTime.parse(json['datetimeCreateUser'])
        : null;
    datetimeLastConnection = json['datetimeLastConnection'] != null
        ? DateTime.parse(json['datetimeLastConnection'])
        : null;
    if (json['hasValidate'] != null) {
      hasValidate = json['hasValidate'];
    } else {
      hasValidate = null;
    }
    if (json['categorie'] != null) {
      categorie = json['categorie'];
    } else {
      categorie = null;
    }
    if (json['importance'] != null) {
      importance = json['importance'];
    } else {
      importance = null;
    }
    if (json['marie'] != null) {
      marie = Marie.fromJson(json['marie']);
    } else {
      marie = null;
    }
    if (json['famille'] != null) {
      famille = Famille.fromJson(json['famille']);
    } else {
      famille = null;
    }
  }

  bool hasValidate;
  String importance;
  String categorie;
  Marie marie;
  Famille famille;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (uuid != null) {
      data['uuid'] = uuid;
    }
    if (firstName != null) {
      data['firstName'] = firstName;
    }
    if (lastName != null) {
      data['lastName'] = lastName;
    }
    if (datetimeCreateUser != null) {
      data['datetimeCreateUser'] = datetimeCreateUser.toString();
    }
    if (datetimeLastConnection != null) {
      data['datetimeLastConnection'] = datetimeLastConnection.toString();
    }
    if (hasValidate != null) {
      data['hasValidate'] = hasValidate;
    }
    if (categorie != null) {
      data['categorie'] = categorie;
    }
    if (importance != null) {
      data['importance'] = importance;
    }
    if (marie != null) {
      data['marie'] = marie.toJson();
    }
    if (famille != null) {
      data['famille'] = famille.toJson();
    }
    if (email != null) {
      data['email'] = email;
    }
    return data;
  }
}
