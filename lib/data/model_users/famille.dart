import 'package:mariage_ui/data/model_users/invites.dart';

class Famille {
  Famille({this.uuid, this.name, this.membres, this.email});

  Famille.fromJson(Map<String, dynamic> json) {
    if (json['uuid'] != null) {
      uuid = json['uuid'];
    } else {
      uuid = null;
    }
    if (json['name'] != null) {
      name = json['name'];
    } else {
      name = null;
    }

    if (json['email'] != null) {
      email = json['email'];
    } else {
      email = null;
    }


    if (json['membres'] != null) {
      membres = [];
      json['membres'].forEach((dynamic v) {
        membres.add(Invite.fromJson(v));
      });
    }
  }

  String uuid;
  String name;
  List<Invite> membres;
  String email;

  Map<String, dynamic> toJson({bool removeMembres = false}) {
    final data = <String, dynamic>{};
    if (uuid != null) {
      data['uuid'] = uuid;
    }
    if (name != null) {
      data['name'] = name;
    }
    if (email != null) {
      data['email'] = email;
    }
    if (membres != null && !removeMembres) {
      data['membres'] = membres.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
