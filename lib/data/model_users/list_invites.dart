

import 'package:mariage_ui/data/model_users/invites.dart';

class ListInvites {


  ListInvites({this.invites});

  ListInvites.fromJson(Map<String, dynamic> json) {
    if (json['invites'] != null) {
      final invites = <Invite>[];
      json['invites'].forEach((dynamic v) {
        invites.add(Invite.fromJson(v));
      });
      this.invites = invites;
    }
  }

  List<Invite> invites;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (invites != null) {
      data['invites'] = invites.map((v) => v.toJson()).toList();
    }
    return data;
  }
}



