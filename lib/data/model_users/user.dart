abstract class User {

  User({
    this.uuid,
    this.firstName,
    this.lastName,
    this.email,
    this.datetimeCreateUser,
    this.datetimeLastConnection
});

  String uuid;
  String firstName;
  String lastName;
  String email;
  DateTime datetimeCreateUser;
  DateTime datetimeLastConnection;
}