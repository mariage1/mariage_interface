import 'package:mariage_ui/data/model_users/list_invites.dart';
import 'package:mariage_ui/data/model_users/user.dart';

class Marie extends User {
  Marie(
      {String uuid,
      String firstName,
      String lastName,
      String email,
      DateTime datetimeCreateUser,
      DateTime datetimeLastConnection,
      this.invites})
      : super(
            uuid: uuid,
            firstName: firstName,
            lastName: lastName,
            email: email,
            datetimeCreateUser: datetimeCreateUser,
            datetimeLastConnection: datetimeLastConnection);

  Marie.fromJson(Map<String, dynamic> json) {
    if (json['uuid'] != null) {
      uuid = json['uuid'];
    } else {
      uuid = null;
    }
    if (json['firstName'] != null) {
      firstName = json['firstName'];
    } else {
      firstName = null;
    }
    if (json['lastName'] != null) {
      lastName = json['lastName'];
    } else {
      lastName = null;
    }
    if (json['email'] != null) {
      email = json['email'];
    } else {
      email = null;
    }
    datetimeCreateUser = json['datetimeCreateUser'] != null
        ? DateTime.parse(json['datetimeCreateUser'])
        : null;
    datetimeLastConnection = json['datetimeLastConnection'] != null
        ? DateTime.parse(json['datetimeLastConnection'])
        : null;
    if (json['invites'] != null) {
      invites = ListInvites.fromJson(json['invites']);
    }
  }

  ListInvites invites;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (uuid != null) {
      data['uuid'] = uuid;
    }
    if (firstName != null) {
      data['firstName'] = firstName;
    }
    if (lastName != null) {
      data['lastName'] = lastName;
    }
    if (email != null) {
      data['email'] = email;
    }
    if (datetimeCreateUser != null) {
      data['datetimeCreateUser'] = datetimeCreateUser.toString();
    }
    if (datetimeLastConnection != null) {
      data['datetimeLastConnection'] = datetimeLastConnection.toString();
    }
    if (invites != null) {
      data['invites'] = invites.toJson();
    }
    return data;
  }
}
