import 'dart:core';

import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mariage_ui/service/logger_service.dart';

class TokenModel {


  TokenModel(this.token, this.type);

  TokenModel.fromJson(Map<String, dynamic> json){
    token = json['token'];
    type = json['type'];
  }

  String token;
  String type;

  Map<String, dynamic> toJson() => {
    'token': token,
    'type': type,
  };

  bool tokenIsValide(){
    LoggerService.instance.debug(JwtDecoder.getExpirationDate(token).toString());
    return !JwtDecoder.isExpired(token);
  }

}
