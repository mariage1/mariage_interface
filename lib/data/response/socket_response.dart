class SocketResponse {

  SocketResponse({this.stat, this.message});

  SocketResponse.fromJson(Map<String, dynamic> json) {
    stat = json['stat'];
    message = json['message'];
  }

  String stat;
  String message;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['stat'] = stat;
    data['message'] = message;
    return data;
  }
}

