class ApiResponse {

  ApiResponse({this.stat, this.message});

  ApiResponse.fromJson(Map<String, dynamic> json) {
    stat = json['stat'];
    message = json['message'];
  }

  bool stat;
  String message;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['stat'] = stat;
    data['message'] = message;
    return data;
  }
}

class ApiLoginResponse {
  ApiLoginResponse.fromJson(Map<String, dynamic> json){
    apiResponse = ApiResponse()
        ..message = json['message']
        ..stat = json['stats'];
    tokenType = json['tokenType'];
    token = json['token'];
  }
  ApiResponse apiResponse;
  String tokenType;
  String token;
}
